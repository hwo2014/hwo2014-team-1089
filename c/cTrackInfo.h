#ifndef _C_TRACK_INFO_H
#define _C_TRACK_INFO_H

#include "common_defines.h"

#define TRACK_ID_LENGTH 512
#define TRACK_NAME_LENGTH 512
#define TRACK_MAX_NUMBER_PIECES 512
#define TRACK_MAX_NUMBER_LANES 512
#define CAR_COLOR_LENGTH 512
#define CAR_NAME_LENGTH 512

#define GAME_ID_LENGTH 512

struct cJSON;

enum EPiece_Type
{
	UNDEFINED,
	STREIGHT,
	CURVE,
	BRIDGE
};

struct StartingPoint_Data
{
	double x, y;
	double angle;

	StartingPoint_Data() :x(-1.0), y(-1.0), angle(-1){}
};

struct Lane_Data
{
	unsigned int	index;
	double			distance_from_center;

	Lane_Data() : index(-1), distance_from_center(-1.0){}
};

struct Piece_Data
{
	EPiece_Type type;
	double		length;
	double		radius;
	double		angle;
	bool		has_switch;

	/*Calculated top speed for this part*/
	double		top_speed; // -1.0  inseamna maxim

	Piece_Data() : type(UNDEFINED), length(-1.0), top_speed(-1.0), radius(-1.0), angle(-1.0), has_switch(false){}
};

/* Pentru mesajul de car positions primit pe update*/
struct cCarPosition
{
	double angle;
	int	   piece_index;
	double in_piece_distance;
	int	   start_lane_index;
	int	   end_lane_index;
	int	   lap;

	/* This position is calculated */
	float distance_along_track;
	
	cCarPosition() : lap(-1),angle(-1.0), piece_index(-1), in_piece_distance(-1.0), start_lane_index(-1), end_lane_index(-1){}
};

struct Car_Data
{
	/* data taken from GameInit message*/
	char car_color_id[CAR_COLOR_LENGTH];
	char car_name_id[CAR_NAME_LENGTH];

	double length;
	double width;
	double guideFlagPosition;

	

	Car_Data() : length(-1.0), width(-1.0), guideFlagPosition(-1.0)
	{
		INIT_STRING(car_color_id);
		INIT_STRING(car_name_id);
	}

	/* Data taken from CarPositions message*/
	cCarPosition position;

	/* User set throttle */
	double throttle;
};

struct RaceSession_Data
{
	int		laps;
	double	max_lap_time_ms;
	bool	quick_race;

	RaceSession_Data(): laps(-1), max_lap_time_ms(-1.0), quick_race(false){}
};

// Race Data parsed from JSON Game Start message
struct TrackInfo_Data
{
	//id comes before cars tinem astea aici
	char				mycar_color[CAR_COLOR_LENGTH];
	char				mycar_name[CAR_NAME_LENGTH];
	///////////////////////////////////////////
	char				id[TRACK_ID_LENGTH];
	char				name[TRACK_NAME_LENGTH];

	Piece_Data			pieces[TRACK_MAX_NUMBER_PIECES];
	unsigned int		number_of_pieces;

	Lane_Data			lanes[TRACK_MAX_NUMBER_LANES];
	unsigned int		number_of_lanes;

	StartingPoint_Data starting_point;

	Car_Data			all_cars[TRACK_MAX_NUMBER_LANES];
	unsigned int		number_of_cars;

	Car_Data* 			my_car;
	Piece_Data*			next_curve;

	RaceSession_Data	race_session;

	char				game_id[GAME_ID_LENGTH];

	double				game_tick;

	TrackInfo_Data() :number_of_pieces(0), number_of_lanes(0), number_of_cars(0), my_car(0), game_tick(0.0), next_curve(0)
	{
		INIT_STRING(id);
		INIT_STRING(name);
		INIT_STRING(game_id);
	}
};


//test functions
void Test_TrackInfo_CarsPositions_Parse();
void Test_TrackInfo_GameInit_Parse();

//Real functions
void TrackInfo_GameInit(TrackInfo_Data*, cJSON *);
void TrackInfo_CarPositions(TrackInfo_Data*, cJSON *);
void TrackInfo_YourCar(TrackInfo_Data*, cJSON*);
void TrackInfo_OutputCSVPositions(TrackInfo_Data*);

double TrackInfo_GetTotalCarDistanceIfNotChangeLanes(TrackInfo_Data*, Car_Data *car);

//Utility Stuff
void Piece_Data_Copy(Piece_Data** dst, Piece_Data* src);
double Piece_Data_GetLaneLength(Piece_Data* pdata, int lane);

#ifdef ENABLE_DATA_RECORDING
void TrackInfo_Record_Init();
void TrackInfo_AddPacket(TrackInfo_Data *data);
void TrackInfo_OutputCSVPositions(TrackInfo_Data* data);
void TrackInfo_Record_Deinit();
#endif

#endif //_C_TRACK_INFO_H