/* E biblioteca de pe arduino de PID  care e decenta refacuta sa fie C-like de mine si scos partile de arduino*/

#include "PID_v1.h"

/*Constructor (...)*********************************************************
 *    The parameters specified here are those for for which we can't set up 
 *    reliable defaults, so we need to have the user set them.
 ***************************************************************************/
void PID_Setup(PID_data* data,double* Input, double* Output, double* Setpoint,
        double Kp, double Ki, double Kd, int ControllerDirection,unsigned long currentTime)
{
	
    data->myOutput = Output;
    data->myInput = Input;
    data->mySetpoint = Setpoint;
	data->inAuto = false;
	
	PID_SetOutputLimits(data,0, 255);				//default output limit corresponds to 
												//the arduino pwm limits

	data->SampleTime = 1;							//default Controller Sample Time is 1 tick

	PID_SetControllerDirection(data,ControllerDirection);
    PID_SetTunings(data,Kp, Ki, Kd);

	data->lastTime = currentTime - data->SampleTime;
}
 
 
/* Compute() **********************************************************************
 *     This, as they say, is where the magic happens.  this function should be called
 *   every time "void loop()" executes.  the function will decide for itself whether a new
 *   pid Output needs to be computed.  returns true when the output is computed,
 *   false when nothing has been done.
 **********************************************************************************/ 
bool PID_Compute(PID_data* data, unsigned long currentTime)
{
   if(!data->inAuto) return false;
   unsigned long now = currentTime;
   unsigned long timeChange = (now - data->lastTime);
   if(timeChange>=data->SampleTime)
   {
      /*Compute all the working error variables*/
	  double input = *data->myInput;
      double error = *data->mySetpoint - input;
      data->ITerm+= (data->ki * error);
      if(data->ITerm > data->outMax) data->ITerm= data->outMax;
      else if(data->ITerm < data->outMin) data->ITerm= data->outMin;
      double dInput = (input - data->lastInput);
 
      /*Compute PID Output*/
	  double output = data->kp * error + data->ITerm - data->kd * dInput;
      
	  if (output > data->outMax) output = data->outMax;
	  else if (output < data->outMin) output = data->outMin;
	  *data->myOutput = output;
	  
      /*Remember some variables for next time*/
	  data->lastInput = input;
	  data->lastTime = now;
	  return true;
   }
   else return false;
}


/* SetTunings(...)*************************************************************
 * This function allows the controller's dynamic performance to be adjusted. 
 * it's called automatically from the constructor, but tunings can also
 * be adjusted on the fly during normal operation
 ******************************************************************************/ 
void PID_SetTunings(PID_data* data,double Kp, double Ki, double Kd)
{
   if (Kp<0 || Ki<0 || Kd<0) return;
 
   data->dispKp = Kp; data->dispKi = Ki; data->dispKd = Kd;
   
   double SampleTimeInSec = ((double)data->SampleTime) / 1000;
   data->kp = Kp;
   data->ki = Ki * SampleTimeInSec;
   data->kd = Kd / SampleTimeInSec;
 
   if (data->controllerDirection == REVERSE)
   {
	   data->kp = (0 - data->kp);
	   data->ki = (0 - data->ki);
	   data->kd = (0 - data->kd);
   }
}
  
/* SetSampleTime(...) *********************************************************
 * sets the period, in Milliseconds, at which the calculation is performed	
 ******************************************************************************/
void PID_SetSampleTime(PID_data * data,int NewSampleTime)
{
   if (NewSampleTime > 0)
   {
      double ratio  = (double)NewSampleTime
		  / (double)data->SampleTime;
	  data->ki *= ratio;
	  data->kd /= ratio;
	  data->SampleTime = (unsigned long)NewSampleTime;
   }
}
 
/* SetOutputLimits(...)****************************************************
 *     This function will be used far more often than SetInputLimits.  while
 *  the input to the controller will generally be in the 0-1023 range (which is
 *  the default already,)  the output will be a little different.  maybe they'll
 *  be doing a time window and will need 0-8000 or something.  or maybe they'll
 *  want to clamp it from 0-125.  who knows.  at any rate, that can all be done
 *  here.
 **************************************************************************/
void PID_SetOutputLimits(PID_data * data,double Min, double Max)
{
   if(Min >= Max) return;
   data->outMin = Min;
   data->outMax = Max;
 
   if(data->inAuto)
   {
	   if (*data->myOutput > data->outMax) *data->myOutput = data->outMax;
	   else if (*data->myOutput < data->outMin) *data->myOutput = data->outMin;
	 
	   if (data->ITerm > data->outMax) data->ITerm = data->outMax;
	   else if (data->ITerm < data->outMin) data->ITerm = data->outMin;
   }
}

/* SetMode(...)****************************************************************
 * Allows the controller Mode to be set to manual (0) or Automatic (non-zero)
 * when the transition from manual to auto occurs, the controller is
 * automatically initialized
 ******************************************************************************/ 
void PID_SetMode(PID_data* data,int Mode)
{
    bool newAuto = (Mode == AUTOMATIC);
	if (newAuto == !data->inAuto)
    {  /*we just went from manual to auto*/
        PID_Initialize(data);
    }
	data->inAuto = newAuto;
}
 
/* Initialize()****************************************************************
 *	does all the things that need to happen to ensure a bumpless transfer
 *  from manual to automatic mode.
 ******************************************************************************/ 
void PID_Initialize(PID_data * data)
{
	data->ITerm = *data->myOutput;
	data->lastInput = *data->myInput;
	if (data->ITerm > data->outMax) data->ITerm = data->outMax;
	else if (data->ITerm < data->outMin) data->ITerm = data->outMin;
}

/* SetControllerDirection(...)*************************************************
 * The PID will either be connected to a DIRECT acting process (+Output leads 
 * to +Input) or a REVERSE acting process(+Output leads to -Input.)  we need to
 * know which one, because otherwise we may increase the output when we should
 * be decreasing.  This is called from the constructor.
 ******************************************************************************/
void PID_SetControllerDirection(PID_data *data,int Direction)
{
	if (data->inAuto && Direction != data->controllerDirection)
   {
		data->kp = (0 - data->kp);
		data->ki = (0 - data->ki);
		data->kd = (0 - data->kd);
   }   
	data->controllerDirection = Direction;
}

/* Status Funcions*************************************************************
 * Just because you set the Kp=-1 doesn't mean it actually happened.  these
 * functions query the internal state of the PID.  they're here for display 
 * purposes.  this are the functions the PID Front-end uses for example
 ******************************************************************************/

