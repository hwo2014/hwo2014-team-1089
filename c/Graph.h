#ifndef GRAPH_STUFF
#define GRAPH_STUFF

struct cNode;
struct cList; 
struct cGraphVertex;

cNode*			cNode_create(int piece_index,  int piece_lane, float piece_cost);
void			cNode_destroy(cNode* node);

cList*			cList_create();
void			cList_destroy(cList* list);
void			cList_tail_add(cList* list, int piece_index, int lane, float cost);

//Practic fiecare vertex e cu lista de adiacenta.
cGraphVertex*	cGraphVertex_create(int piece_index, int piece_lane, float piece_cost);
void			cGraphVertex_destroy(cGraphVertex* vertex);
bool			cGraphVertex_matches(cGraphVertex* vertex, int piece_index, int piece_lane);
//Aici adaug un nou element in lista lui de adiacenta
void			cGraphVertex_add_link(cGraphVertex* vertex_from, cGraphVertex* vertex_to);


void			Dijkstra(cGraphVertex* start_vertex, cGraphVertex* end_vertex);

struct TrackVertexData
{
	cGraphVertex*		vertices[512];
	int					vertices_count;
};

void			TrackVertexData_free(TrackVertexData* data);
int				find_vertex_idx( TrackVertexData* vertex_data, int piece_idx, int lane_idx, bool create_if_missing);

#endif