#include "cTrackInfo.h"
#include "Utils.h"
#include "cJSON.h"
#include <math.h>
#include <string.h>

TrackInfo_Data g_trackData;

#ifdef ENABLE_DATA_RECORDING
#include "cCarController.h"

struct Data_Record
{
	double GameTick;
	double Distance;
	double Throttle;
	double InstantSpeed;
	double InstantAngleSpeed;
};

extern CarController_Data	g_myCarControllerData;

Data_Record * g_recording;
int				number_of_packets;


void TrackInfo_Record_Init()
{
	g_recording = (Data_Record*)malloc(SIZE_OF_RECORDING * sizeof(Data_Record));
	number_of_packets = 0;
}

void TrackInfo_AddPacket(TrackInfo_Data *data)
{
	if (data->my_car != NULL)
	{
		Data_Record rec;
		rec.Distance = TrackInfo_GetTotalCarDistanceIfNotChangeLanes(data, data->my_car);
		rec.GameTick = data->game_tick;
		rec.Throttle = g_myCarControllerData.throttle;
		rec.InstantSpeed = g_myCarControllerData.computed_instant_speed;
		rec.InstantAngleSpeed = g_myCarControllerData.computed_instant_angle_speed;
		g_recording[number_of_packets++] = rec;
	}
}

void TrackInfo_OutputCSVPositions(TrackInfo_Data* data)
{
	char fname[FILE_NAME_SIZE_RECORD];
	sprintf(fname, "%sRecord_%s.csv", RECORD_FILE_OUTPUT_FOLDER, data->game_id);
	FILE* f = fopen(fname, "w");
	if (f != NULL)
	{
		fprintf(f, "Distance , GameTick , Throttle, Instant_Speed, Instant_Angle_Speed \n");
		for (int i = 0; i < number_of_packets; i++)
		{
			Data_Record &r = g_recording[i];
			fprintf(f, "%f , %f , %f ,%f ,%f \n", r.Distance, r.GameTick, r.Throttle, r.InstantSpeed,r.InstantAngleSpeed);
		}
		fclose(f);
	}
}
void TrackInfo_Record_Deinit()
{
	free(g_recording);
}
#endif

//helper function -> Oare sa facem un map string -> idx ca sa facem cautarea mai rapida?.... cat de multe masini o sa fie maxim oare?
Car_Data *TrackInfo_GetCarById(TrackInfo_Data *track,const char* name, const char* color)
{
	for (unsigned int i = 0; i < track->number_of_cars; i++)
	{
		Car_Data & d = track->all_cars[i];
		if ((strcmp(d.car_color_id, color) == 0) && (strcmp(d.car_name_id, name) == 0))
		{
			return &d;
		}
	}
	return NULL;
}

void Test_TrackInfo_GameInit_Parse()
{
#ifdef WRITE_TO_FILE
	char* buff = ReadFileToString(READ_FILE_PATH(TrackInfo));

	cJSON *json = cJSON_Parse(buff);
	TrackInfo_GameInit(&g_trackData, json);

	free(buff);
	free(json);
#endif
}


void Test_TrackInfo_CarsPositions_Parse()
{
#ifdef WRITE_TO_FILE
	char* buff = ReadFileToString(READ_FILE_PATH(CarPositions));

	cJSON *json = cJSON_Parse(buff);
	TrackInfo_CarPositions(&g_trackData, json);

	free(buff);
	free(json);
#endif
}

void TrackInfo_CarPositions(TrackInfo_Data* data, cJSON *json)
{
	cJSON * msg_type = cJSON_GetObjectItem(json, "msgType");

	if (msg_type == NULL)
		return;

	if (strcmp(msg_type->valuestring, "carPositions") != 0)
		return;
	
	cJSON * dataf = cJSON_GetObjectItem(json, "data");
	// all cars
	for (cJSON *el = dataf->child; el != NULL; el = el->next)
	{
		cJSON * _id = cJSON_GetObjectItem(el, "id");
		cJSON * _name = cJSON_GetObjectItem(_id, "name");
		cJSON * _color = cJSON_GetObjectItem(_id, "color");

		Car_Data* cd = TrackInfo_GetCarById(data, _name->valuestring, _color->valuestring);
		
		cJSON * _angle = cJSON_GetObjectItem(el, "angle");
		cJSON * _piecePosition = cJSON_GetObjectItem(el, "piecePosition");

		cJSON * _pieceIndex = cJSON_GetObjectItem(_piecePosition, "pieceIndex");
		cJSON * _inPieceDistance = cJSON_GetObjectItem(_piecePosition, "inPieceDistance");
		cJSON * _lane = cJSON_GetObjectItem(_piecePosition, "lane");
		cJSON * _startLaneidx = cJSON_GetObjectItem(_lane, "startLaneIndex");
		cJSON * _endLaneidx = cJSON_GetObjectItem(_lane, "endLaneIndex");
		
		cJSON * lap = cJSON_GetObjectItem(_piecePosition, "lap");

		cd->position.angle = _angle->valuedouble;
		cd->position.end_lane_index = _endLaneidx->valueint;
		cd->position.start_lane_index = _startLaneidx->valueint;
		cd->position.in_piece_distance = _inPieceDistance->valuedouble;
		cd->position.piece_index = _pieceIndex->valueint;
		
		if (lap)
			cd->position.lap = lap->valueint;

		int current_idx = (cd->position.piece_index +1 )% data->number_of_pieces;
		for (int i = 0; i < data->number_of_pieces; i++)
		{
			if (data->pieces[current_idx].type == CURVE)
			{
				data->next_curve = &data->pieces[current_idx];
				break;
			}
			current_idx = (current_idx + 1) % data->number_of_pieces;
		}
	}


#ifdef ENABLE_DATA_RECORDING
	TrackInfo_AddPacket(data);
#endif
}

void TrackInfo_YourCar(TrackInfo_Data * data, cJSON* json)
{
	cJSON * msg_type = cJSON_GetObjectItem(json, "msgType");
	if (msg_type == NULL)
		return;

	if (strcmp(msg_type->valuestring, "yourCar") != 0)
		return;
	
	cJSON * dataf = cJSON_GetObjectItem(json, "data");
	cJSON * name = cJSON_GetObjectItem(dataf, "name");
	cJSON * color = cJSON_GetObjectItem(dataf, "color");

	data->my_car = TrackInfo_GetCarById(data, name->valuestring, color->valuestring);
	
	strcpy(data->mycar_color, color->valuestring);
	strcpy(data->mycar_name, name->valuestring);
}

void TrackInfo_GameInit(TrackInfo_Data* data, cJSON * json)
{
	cJSON * msg_type = cJSON_GetObjectItem(json, "msgType");
	if (msg_type == NULL)
		return;
	
	if (strcmp(msg_type->valuestring, "gameInit") != 0)
		return;

	cJSON * dataf = cJSON_GetObjectItem(json, "data");
	cJSON * race = cJSON_GetObjectItem(dataf, "race");
	cJSON * track = cJSON_GetObjectItem(race, "track");
	cJSON *id = cJSON_GetObjectItem(track, "id");
	strcpy(data->id, id->valuestring);
	
	cJSON *name = cJSON_GetObjectItem(track, "name");
	strcpy(data->name, name->valuestring);
	
	cJSON *pieces = cJSON_GetObjectItem(track, "pieces");
	int num_pieces = 0;
	for (cJSON *el = pieces->child; el != NULL; el = el->next)
	{
		cJSON* len = cJSON_GetObjectItem(el, "length");
		cJSON* sw = cJSON_GetObjectItem(el, "switch");
		cJSON* ang = cJSON_GetObjectItem(el, "angle");
		cJSON* rad = cJSON_GetObjectItem(el, "radius");
		
		Piece_Data &p = data->pieces[num_pieces];
		
		if (len != NULL)
			p.length = len->valuedouble;
		if (sw != NULL)
			p.has_switch = true;
		if (ang != NULL)
			p.angle = ang->valuedouble;
		
		if (rad != NULL)
		{
			p.radius = rad->valuedouble;
			p.type = CURVE;
		}
		else
		{
			p.type = STREIGHT;
		}

	
		num_pieces++;
					
	}
	
	data->number_of_pieces = num_pieces;
	
	cJSON * lanes = cJSON_GetObjectItem(track, "lanes");
	int number_lanes = 0;
	
	// we need to keep the lanes sorted (the .index is the actual array index)
	for (cJSON *el = lanes->child; el != NULL; el = el->next)
	{
		cJSON* dstCenter = cJSON_GetObjectItem(el, "distanceFromCenter");
		cJSON* idx = cJSON_GetObjectItem(el, "index");
		int index = idx->valueint;
		
		Lane_Data &d = data->lanes[index];
		
		d.distance_from_center = dstCenter->valuedouble;
		d.index = idx->valueint;
		if (index > number_lanes)
		{
			number_lanes = index+1;
		}
	}
	
	data->number_of_lanes = number_lanes;
	cJSON * sPoint = cJSON_GetObjectItem(track, "startingPoint");
	cJSON * sPosition = cJSON_GetObjectItem(sPoint, "position");

	cJSON * sPositionX = cJSON_GetObjectItem(sPosition, "x");
	cJSON * sPositionY = cJSON_GetObjectItem(sPosition, "y");
	cJSON * sAngle = cJSON_GetObjectItem(sPoint, "angle");
	data->starting_point.angle = sAngle->valuedouble;
	data->starting_point.x = sPositionX->valuedouble;
	data->starting_point.y = sPositionY->valuedouble;

	cJSON * thecars = cJSON_GetObjectItem(race, "cars");
	int number_cars = 0;
	for (cJSON *el = thecars->child; el != NULL; el = el->next)
	{
		Car_Data &cd = data->all_cars[number_cars];

		cJSON* carid =cJSON_GetObjectItem(el, "id");
		cJSON* cardim  = cJSON_GetObjectItem(el, "dimensions");
		cJSON* carname = cJSON_GetObjectItem(carid, "name");
		cJSON* carcolor = cJSON_GetObjectItem(carid, "color");
		cJSON* len = cJSON_GetObjectItem(cardim, "length");
		cJSON* wid = cJSON_GetObjectItem(cardim, "width");
		cJSON* gfp = cJSON_GetObjectItem(cardim, "guideFlagPosition");
		
		strcpy(cd.car_color_id, carcolor->valuestring);
		strcpy(cd.car_name_id, carname->valuestring);
		cd.guideFlagPosition = gfp->valuedouble;
		cd.length = len->valuedouble;
		cd.width = wid->valuedouble;
		number_cars++;
	}
	data->number_of_cars = number_cars;

	cJSON * rsession = cJSON_GetObjectItem(race, "raceSession");
	cJSON * laps= cJSON_GetObjectItem(rsession, "laps");
	cJSON  *masLapTime = cJSON_GetObjectItem(rsession, "maxLapTimeMs");
	cJSON  *qrace = cJSON_GetObjectItem(rsession, "quickRace");
	
	if (laps)
		data->race_session.laps = laps->valueint;
	if (masLapTime)
		data->race_session.max_lap_time_ms = masLapTime->valuedouble;
	if (qrace)
		data->race_session.quick_race = (bool)qrace->valueint;

	strcpy(data->game_id, cJSON_GetObjectItem(json, "gameId")->valuestring);

	// get my car
	if (data->my_car == 0)
		data->my_car = TrackInfo_GetCarById(data, data->mycar_name, data->mycar_color);

}


double TrackInfo_GetTotalCarDistanceIfNotChangeLanes(TrackInfo_Data* tdata, Car_Data *car)
{
	double distanc_total_track = 0.0f;
	double distance_util_now = 0.0;

	for (unsigned int i = 0; i < tdata->number_of_pieces; i++)
	{
		Piece_Data & d = tdata->pieces[i];
		
		double piece_distance = 0.0;
		switch (d.type)
		{
			case STREIGHT:
			{
				piece_distance = d.length;
				break;
			}
			case CURVE:
			{
				double absAngle = abs(d.angle);
				double arc_radius = d.radius + tdata->lanes[car->position.start_lane_index].distance_from_center * (-SGN(d.angle));
				piece_distance = GET_ARC_LENGTH(absAngle,arc_radius );
				break;
			}
		}
		if (i < car->position.piece_index)
		{
			distance_util_now += piece_distance;
		}
		distanc_total_track += piece_distance;
	}
	distance_util_now += car->position.in_piece_distance;
	distance_util_now += distanc_total_track * car->position.lap;
	return distance_util_now;
}

double Piece_Data_GetLaneLength(Piece_Data* pdata, int lane)
{
	double piece_distance = 0.0;
	switch (pdata->type)
	{
		case STREIGHT:
			{
				piece_distance = pdata->length;
				break;
			}
		case CURVE:
			{
				double absAngle = abs(pdata->angle);
				double arc_radius = pdata->radius + g_trackData.lanes[lane].distance_from_center * (-SGN(pdata->angle));
				piece_distance = GET_ARC_LENGTH(absAngle,arc_radius);
				break;
			}
	}

	return piece_distance;
}

void Piece_Data_Copy(Piece_Data** dst, Piece_Data* src)
{
	Piece_Data* data = (Piece_Data*)malloc(sizeof(Piece_Data));
	memcpy(data, src, sizeof(Piece_Data));
	dst = &data;
}
