/*******************INCLUDES************************/
#include <errno.h>


#ifdef WIN32
#define WRITE_TO_FILE
#endif

#ifndef WIN32
	#include <netdb.h>
	#include <sys/socket.h>
	#include <unistd.h>
#else
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <assert.h>
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>


#define GET_ARC_LENGTH(DEG,RADIUS) (2* PI * (RADIUS) * (DEG)/360.0)
#define SGN(x) (((x)>0) ? (1) : (-1))
// trebuie pi-ul nostru
#define PI 3.1415926535897932384626433832795 

#ifdef WRITE_TO_FILE
#define ENABLE_DATA_RECORDING
#endif

#ifdef ENABLE_DATA_RECORDING
	#define SIZE_OF_RECORDING 4096
	#define RECORD_FILE_OUTPUT_FOLDER "ExportCSVs/" 
	#define FILE_NAME_SIZE_RECORD 1024
#endif

/**************MESSAGES DEBUG DEFINES **************/
#define DEBUG_MESSAGES_FOLDER "ParseTestFiles/"
#define PARSE_EXTENSION  ".txt"

#define CONCAT_PATH(x,y) x y
#define READ_FILE_PATH_(x) CONCAT_PATH(DEBUG_MESSAGES_FOLDER,#x)
#define READ_FILE_PATH(x) CONCAT_PATH(READ_FILE_PATH_(x),PARSE_EXTENSION)

/**************LOGGER DEFINES**********************/
#ifdef WRITE_TO_FILE
#define DEBUG_LOG_JSON_TO_FILE
#define DEBUG_FILE_TO_SCREEN
#define LOG_LINE_SIZE 2048

#define DEBUG_OUTPUTLOG_FOLDER "OutputLogs/ie "
#define DEBUG_OUTPUTLOG_FILENAME "OutputLog"
#endif
/**************LOGGER************************************/
#ifdef DEBUG_LOG_JSON_TO_FILE

	int LOG_Init(const char* fname);
	void _LOG(char *fmt, ...);
	void LOG_Deinit();

	#define LOGINIT(x) LOG_Init(x)
	#define LOGWRITE(x, ...) _LOG(x, __VA_ARGS__)
	#define LOGCLEANUP() LOG_Deinit()

#else
	#define LOGINIT(x) 
	#define LOGWRITE(x, ...) printf(x, __VA_ARGS__)
	#define LOGCLEANUP()
#endif

/***************MISC********************************/
char * ReadFileToString(const char * filename);