#include "Utils.h"

#include "cTrackInfo.h"
#include "cCarController.h"
#include "cJSON.h"
#include "Graph.h"

#pragma comment(lib, "Ws2_32.lib")

extern TrackInfo_Data		g_trackData;
extern CarController_Data	g_myCarControllerData;

TrackVertexData g_TrackVertexData;

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *switchLane_msg(const char* LeftRight);
static cJSON *create_race_msg(char* bot_name, char* bot_key, char* track_name, char* password, int car_count);
static cJSON *join_race_msg(char* bot_name, char* bot_key, char* track_name, char* password, int car_count);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}


void TestGraphStuff()
{
	for (int current_piece = 0; current_piece < g_trackData.number_of_pieces; current_piece++)
	{
		for (int lane = 0; lane < g_trackData.number_of_lanes; lane++)
		{
			int current_vertex_index = find_vertex_idx(&g_TrackVertexData, current_piece, lane, true);
			cGraphVertex* vertex = g_TrackVertexData.vertices[current_vertex_index];
			for (int nextLane = lane-1; nextLane <= lane+1; nextLane++)
			{
				if(nextLane < 0 || nextLane >= g_trackData.number_of_lanes)
					continue;
				int next_piece_idx = (current_piece + 1) % g_trackData.number_of_pieces;
				int found_vtx = find_vertex_idx(&g_TrackVertexData, next_piece_idx, nextLane, true);

				cGraphVertex_add_link(vertex, g_TrackVertexData.vertices[found_vtx]);
			}
		}
	}

	if(g_TrackVertexData.vertices_count > 0)
		Dijkstra(g_TrackVertexData.vertices[0], g_TrackVertexData.vertices[g_TrackVertexData.vertices_count-1]);

	TrackVertexData_free(&g_TrackVertexData);
}

int main(int argc, char *argv[])
{
	LOGINIT("TrackLog");
#ifdef ENABLE_DATA_RECORDING
	TrackInfo_Record_Init();
#endif
	//Test_TrackInfo_GameInit_Parse();
	//Test_TrackInfo_CarsPositions_Parse();
	/* chem cacatul de initializare de la microsoft pentru socketi*/
#ifdef WIN32
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}
#endif
	printf("Inceput\n");
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

	printf("Fac socket\n");
    sock = connect_to(argv[1], argv[2]);
	printf("Am facut socket\n");
	//TrackNames: 
	//keimola
	//germany
	printf("Creez Join Message\n");
	json = /*create_race_msg*/join_race_msg(argv[3], argv[4], "keimola", "", 1);
	printf("Am Creeat\n");
	printf("Trimit\n");
	write_msg(sock, json);
	printf("Am trimis\n");
    cJSON_Delete(json);
	printf("Sterg json si incep sa citesc de pe socket\n");
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;
		printf("Am mesaj\n");
        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");
		
		cJSON* gameTick = cJSON_GetObjectItem(json, "gameTick");
		
		if (gameTick != NULL)
		{
			g_trackData.game_tick = gameTick->valueint;
		}

        msg_type_name = msg_type->valuestring;
		
		
		if (!strcmp("gameInit", msg_type_name))
		{
			TrackInfo_GameInit(&g_trackData, json);
			
			//TestGraphStuff();
		}
		
		if (!strcmp("yourCar", msg_type_name))
		{
			TrackInfo_YourCar(&g_trackData, json);
		}

		if (!strcmp("carPositions", msg_type_name)) 
		{
			TrackInfo_CarPositions(&g_trackData, json);
			CarController_Update(&g_trackData, &g_myCarControllerData);

			msg = throttle_msg(g_myCarControllerData.throttle);
        } 
		else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

		if (!strcmp("carPositions", msg_type_name))
		{
			switch (g_myCarControllerData.switchL)
			{
				case E_SWITCH_LEFT:
				{
						cJSON *msgmsg = switchLane_msg("Left");
						write_msg(sock, msgmsg);
						cJSON_Delete(msgmsg);
						break;
				}
				case E_SWITCH_RIGHT:
				{
						cJSON *msgmsg = switchLane_msg("Right");
						write_msg(sock, msgmsg);
						cJSON_Delete(msgmsg);
						break;
				}
			}
			
		}

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }
	/*chem cacatul de deinitializare de la microsoft*/
#ifdef WIN32
	WSACleanup();
#endif
	LOGCLEANUP();
#ifdef ENABLE_DATA_RECORDING
	TrackInfo_OutputCSVPositions(&g_trackData);
	TrackInfo_Record_Deinit();
#endif
	return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *create_race_msg(char* bot_name, char* bot_key, char* track_name, char* password, int car_count)
{
	cJSON* data = cJSON_CreateObject();

	cJSON* botId = cJSON_CreateObject();
	cJSON_AddStringToObject(botId, "name", bot_name);
	cJSON_AddStringToObject(botId, "key", bot_key);
	
	cJSON_AddItemToObject(data, "botId", botId);
	cJSON_AddStringToObject(data, "trackName", track_name);
	cJSON_AddStringToObject(data, "password", password);
	
	cJSON_AddItemToObject(data, "carCount", cJSON_CreateNumber(car_count));
	
	return make_msg("createRace", data);
}

static cJSON *join_race_msg(char* bot_name, char* bot_key, char* track_name, char* password, int car_count)
{
	cJSON* data = cJSON_CreateObject();


	cJSON_AddStringToObject(data, "name", bot_name);
	cJSON_AddStringToObject(data, "key", bot_key);

	return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *switchLane_msg(const char* LeftRight)
{
	return make_msg("switchLane", cJSON_CreateString(LeftRight));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = (char*)malloc(bufsz * sizeof(char));
	
	int recv_status = 0;

	while (
#ifndef WIN32
		recv_status = read(fd, readp, 1)
#else
		recv_status = recv(fd, readp, 1, 0)
#endif
> 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = (char*)realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }

	if (json != NULL)
	{
		char *msg_str;
		msg_str = cJSON_PrintUnformatted(json);
		LOGWRITE("Primesc : JSON -> %s \n", msg_str);
		free (msg_str);
	}
	//assert(json != NULL);
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);
	LOGWRITE("Trimit : JSON -> %s \n", msg_str);
#ifndef WIN32
    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);
#else
	int result = 0;
	result = send(fd, msg_str, strlen(msg_str), 0);
	result = send(fd, &nl, 1,0);
#endif
    free(msg_str);
}
