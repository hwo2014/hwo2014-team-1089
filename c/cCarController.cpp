#include "Utils.h"
#include "cCarController.h"
#include "cTrackInfo.h"
#include "PID_v1.h"
#include <math.h>

CarController_Data g_myCarControllerData;

enum ECAR_STATES
{
	E_NONE,
	E_DETERMINE_TRACK_FRICTION_ACCELERATE,
	E_DETERMINE_TRACK_FRICTION_STOP,
	E_RACE
};

#define CALCULATE_THRESHOLD_SPEED_FRICTION (1.0)
#define UNIVERSAL_G_ACCELERATION (9.81)
#define ZERO_SPEED_EPSILON (0.001)

struct CarController_PrivateData
{
	ECAR_STATES state;

	double determined_friction;

	double top_speed;
	double accel_distance;
	double stop_distance;
	double stop_time;
	double prev_distance;
	double prev_angle;

	/* PID stuff */
	PID_data the_pid;
	double SetPoint, Input, Output;

	CarController_PrivateData() :prev_distance(0.0), state(E_NONE), top_speed(0.0), determined_friction(0.0)
	{
		double aggKp = 255, aggKi = 20.0, aggKd =1.0;
		PID_Setup(&the_pid, &Input, &Output, &SetPoint, aggKp, aggKi, aggKd, DIRECT, 1);
		PID_SetMode(&the_pid, AUTOMATIC);
	}
}ccprvData;

void SwitchStates();
/*State Update functions */
int Update_DetermineTrackFriction_accelerate(TrackInfo_Data* track_data, CarController_Data * ccdata);
int Update_DetermineTrackFriction_stop(TrackInfo_Data* track_data, CarController_Data * ccdata);
int Update_Race(TrackInfo_Data* track_data, CarController_Data * ccdata);
/**********Main Update Function**************/
void UpdateStates(TrackInfo_Data* track_data, CarController_Data * ccdata);
/***********Decide Switch lanes function*****/
void Decide_Should_SwitchLanes(TrackInfo_Data *track_data, CarController_Data *ccdata);
/********************************************/

void Decide_Should_SwitchLanes(TrackInfo_Data *track_data, CarController_Data *ccdata)
{
	// TODO : Replace with smarter algorithm
	if (track_data->next_curve)
	{

		float dst = track_data->lanes[track_data->my_car->position.end_lane_index].distance_from_center;
		
		double Radius = track_data->next_curve->radius + track_data->lanes[track_data->my_car->position.end_lane_index].distance_from_center * (-SGN(track_data->next_curve->angle));

		track_data->next_curve->top_speed = sqrt( 0.40 * Radius) ;
		LOGWRITE("Determined top Speed for piece ---> %f \n", track_data->next_curve->top_speed);
		if (track_data->next_curve->angle > 0 && dst < 0)
		{
			ccdata->switchL = E_SWITCH_RIGHT;
		}
		else if (track_data->next_curve->angle < 0 && dst > 0)
		{
			ccdata->switchL = E_SWITCH_LEFT;
		}
		else
		{
			ccdata->switchL = E_SWITCH_NONE;
		}
	}
}

int Update_DetermineTrackFriction_accelerate(TrackInfo_Data* track_data, CarController_Data * ccdata)
{
	ccdata->throttle = 1.0;
	if (ccdata->computed_instant_speed > CALCULATE_THRESHOLD_SPEED_FRICTION)
	{
		ccprvData.top_speed = ccdata->computed_instant_speed;
		ccprvData.accel_distance = ccprvData.prev_distance;
		ccprvData.stop_time = track_data->game_tick;
		return 1;
	}
	return 0;
}

int Update_DetermineTrackFriction_stop(TrackInfo_Data* track_data, CarController_Data * ccdata)
{
	ccdata->throttle = 0.0;
	if (ccdata->computed_instant_speed - ZERO_SPEED_EPSILON <= 0.0)
	{
		ccprvData.stop_distance = ccprvData.prev_distance - ccprvData.accel_distance;
		ccprvData.stop_time = track_data->game_tick - ccprvData.stop_time;
		/*Formula e determinata de mine s-ar putea sa fie gresita :)) : miu = (d-v_0*t)/(1.0/2.0 * g * t^2)  unde v_0 este viteza initiala la care a ajuns d e distanta de oprire*/
		/* ca sa o determin am luat d = v_0 * t + 1/2 *at^2  si am inlocuit a cu -miu* g */

		ccprvData.determined_friction = -(ccprvData.stop_distance - ccprvData.top_speed *ccprvData.stop_time) / (1.0 / 2.0 * UNIVERSAL_G_ACCELERATION * ccprvData.stop_time*ccprvData.stop_time);
		LOGWRITE("Determined friction ---> %f \n", ccprvData.determined_friction);
		return 1;
	}
	return 0;
}

int Update_Race(TrackInfo_Data* track_data, CarController_Data * ccdata)
{
	/*PID UPDATE*/
	if (track_data->next_curve)
		ccprvData.SetPoint = track_data->next_curve->top_speed;
	else
		ccprvData.SetPoint = 7.0; // daca ai throttle 0.5 am observat pe serverul de test viteza maxima 5, vreau sa vad daca poti imbunatati timpul de raspuns cu PID.. adica sa ajunga 5 viteza mai repede si peurma sa se stabilizeze
	
	ccprvData.Input = ccdata->computed_instant_speed;
	PID_Compute(&ccprvData.the_pid, (unsigned long)track_data->game_tick + 2);

	// Trebuie sa scalam outputul , el este la 255 
	ccdata->throttle = ccprvData.Output / 255.0;



	Decide_Should_SwitchLanes(track_data, ccdata);

	return 0;
}

void UpdateStates(TrackInfo_Data* track_data, CarController_Data * ccdata)
{
	int res = 0;
	switch (ccprvData.state)
	{
		case E_NONE:
		{
				res = 1;
				break;
		}
		case E_DETERMINE_TRACK_FRICTION_ACCELERATE:
		{
				res = Update_DetermineTrackFriction_accelerate(track_data,ccdata);
				break;
		}
		case E_DETERMINE_TRACK_FRICTION_STOP:
		{
				res = Update_DetermineTrackFriction_stop(track_data, ccdata);
				break;
		}
		case E_RACE:
		{
				res = Update_Race(track_data, ccdata);
				break;
		}

	}
	if (res == 1)
		SwitchStates();
}
void SwitchStates()
{
	switch (ccprvData.state)
	{
		case E_NONE:
		{
					   ccprvData.state = E_RACE
						   ;
			break;
		}
		case E_DETERMINE_TRACK_FRICTION_ACCELERATE:
		{
			ccprvData.state = E_DETERMINE_TRACK_FRICTION_STOP;
			break;
		}
		case E_DETERMINE_TRACK_FRICTION_STOP:
		{
			ccprvData.state = E_RACE;
			// DO friction loop  to determine friction multiple time to know if it's good
			//ccprvData.state = E_DETERMINE_TRACK_FRICTION_ACCELERATE;
			break;
		}

	}
}

void Calculate_InstantSpeed(TrackInfo_Data* track_data, CarController_Data * ccdata)
{
	double currentDistance = TrackInfo_GetTotalCarDistanceIfNotChangeLanes(track_data, track_data->my_car);
	ccdata->computed_instant_speed = (currentDistance - ccprvData.prev_distance); // timpul e in tickuri si asta e apelata pe 1 tick deci nu tre sa impartim decat in minte la 1
	ccdata->computed_instant_angle_speed = (track_data->my_car->position.angle - ccprvData.prev_angle);
	ccprvData.prev_distance = currentDistance;
	ccprvData.prev_angle = track_data->my_car->position.angle;

}

void CarController_Update(TrackInfo_Data* track_data, CarController_Data * ccdata)
{
	Calculate_InstantSpeed(track_data,ccdata);

	UpdateStates(track_data, ccdata);
}
