#include "Utils.h"
#ifdef  WRITE_TO_FILE
FILE *LogToFile = NULL;
int LOG_Init(const char* fname)
{
	char fileName[1024];
	sprintf(fileName, "%s%s_%s.txt", DEBUG_OUTPUTLOG_FOLDER, DEBUG_OUTPUTLOG_FILENAME, fname);
	LogToFile = fopen(fileName, "w");
	if (LogToFile != NULL)
		return 0;

	return 1;
}

void _LOG(char *fmt, ...)
{
	char buf[LOG_LINE_SIZE];

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, LOG_LINE_SIZE, fmt, ap);
	va_end(ap);

#ifdef DEBUG_FILE_TO_SCREEN
	puts(buf);
#endif
	fputs(buf, LogToFile);
}

void LOG_Deinit()
{
	if (LogToFile != NULL)
	{
		fclose(LogToFile);
	}
}

char * ReadFileToString(const char * filename)
{
	FILE *fp = fopen(filename, "r");

	if (fp != NULL)
	{
		char* buff_source = NULL;

		if (fseek(fp, 0L, SEEK_END) == 0)
		{
			long bufsize = ftell(fp);
			if (bufsize == 0)
			{
				LOGWRITE("---ERROR READING FILE-- %s", filename);
				return NULL;
			}

			buff_source = (char*)malloc(bufsize + 1);
			if (fseek(fp, 0L, SEEK_SET) != 0)
			{
				LOGWRITE("---ERROR READING FILE-- %s", filename);
				return NULL;
			}
			size_t newLen = fread(buff_source, 1, bufsize, fp);
			if (newLen == 0)
			{
				LOGWRITE("---ERROR READING FILE-- NOT ENOUGH MEMORY FOR %s", filename);
				return NULL;
			}
		
		}
		fclose(fp);
		return buff_source;
	}
	else
	{
		LOGWRITE("---ERROR READING FILE-- CANNOT FIND FILE %s", filename);
		return NULL;
	}
}
#endif