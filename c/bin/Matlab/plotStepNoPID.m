%M = csvread('..\ExportCSVs\Record_90d27a91-4e44-4e54-9769-9538ec1cd5cd_faraPID.csv',1,0)
clear all
close all

M = dlmread ('..\ExportCSVs\Record_650f9896-eedd-4039-b5de-c26549c8220e_franatOprit.csv', ',', 1, 0)

figure
hold on
plot(M(:,2),M(:,4))
xlabel('timp')
ylabel('viteza')
title('Raspunsul sistemului la acceleratie constanta 0.5, in viteza')
hold off

