enum ESWITCH_LANE
{
	E_SWITCH_LEFT,
	E_SWITCH_RIGHT,
	E_SWITCH_NONE
};

struct CarController_Data
{
	double throttle;
	double computed_instant_speed;
	double computed_instant_angle_speed;
	ESWITCH_LANE switchL;

	CarController_Data() :throttle(0.0), computed_instant_speed(0.0), computed_instant_angle_speed(0.0){}
};
struct TrackInfo_Data;

void CarController_Update(TrackInfo_Data*,CarController_Data *);
