#include "Graph.h"
#include "stdlib.h"
#include "memory.h"
#include "cTrackInfo.h"

extern	TrackInfo_Data		g_trackData;
#define FLT_MAX				3.402823466e+38F
#define INFINITY			FLT_MAX

struct cNode
{
	cNode*			next;
	cNode*			prev;
	void*			data;

	typedef			void (*data_free_func_)(void*);
	data_free_func_	data_free;
};


struct cList
{
	cNode*			head;
	cNode*			tail;
	int				size;
};


struct cGraphVertex
{
	cList*			list;
	cGraphVertex*	parent;

	int				index;
	int				lane;
	float			cost;
	bool			visited;

	float			total_cost;
};


cNode* cNode_create(void* data)
{
	cNode* node = (cNode*)malloc(sizeof(cNode));
	node->data	= data;
	node->next	= NULL;
	node->prev	= NULL;
	node->data_free = NULL;//free;

	return node;
}


void cNode_destroy(cNode* node)
{
	if(node->data_free)
		node->data_free(node->data);
	free(node);
}


void cNode_remove_from_list(cList* list, cNode* node)
{
	if(node->prev)
		node->prev->next = node->next;
	if(node->next)
		node->next->prev = node->prev;

	if (list->head == node)
		list->head = list->head->next;
	if(list->tail == node)
		list->tail = list->tail->prev;

	node->prev = NULL;
	node->next = NULL;
}



cList* cList_create( )
{
	cList* list = (cList*)malloc(sizeof(cList));
	list->head = NULL;
	list->tail = NULL;
	list->size = 0;

	return list;
}


void cList_destroy(cList* list)
{
	cNode* node = list->head;
	while(node)
	{
		cNode* next_node = node->next;
		cNode_destroy(node);
		node = next_node;
	}

	free(list);
}


int cList_size(cList* list)
{
	int size = 0;
	cNode* currNode = list->head;
	while (currNode != NULL)
	{
		size++;
		currNode = currNode->next;
	}

	return size;
}



void cList_tail_add(cList* list, void* data)
{
	cNode* node = cNode_create(data);

	if (list->head == NULL)
	{
		list->head = node;
		list->tail = node;
	}
	else
	{
		list->tail->next = node;
		node->prev	= list->tail;
		list->tail = node;
	}
}



cGraphVertex* cGraphVertex_create(int piece_index, int piece_lane, float piece_cost)
{
	cGraphVertex* vertex = (cGraphVertex*)malloc(sizeof(cGraphVertex));
	vertex->index = piece_index;
	vertex->lane  = piece_lane;
	vertex->cost  = piece_cost;
	vertex->total_cost = INFINITY;
	vertex->visited	= false;

	vertex->list = cList_create();
	vertex->parent = NULL;

	return vertex;
}


void cGraphVertex_destroy(cGraphVertex* vertex)
{
	//cList_destroy(vertex->list);
	free(vertex->list);
	free(vertex);
}


void cGraphVertex_add_link(cGraphVertex* vertex_from, cGraphVertex* vertex_to)
{
	cList_tail_add(vertex_from->list, vertex_to);
}


bool cGraphVertex_matches(cGraphVertex* vertex, int piece_index, int piece_lane)
{
	return vertex->index == piece_index && vertex->lane == piece_lane;
}


void TrackVertexData_free(TrackVertexData* data)
{
	for (int i = 0; i < data->vertices_count; ++i)
	{
		cGraphVertex_destroy(data->vertices[i]);
	}
	memset(data->vertices, 0, sizeof(data->vertices));
	data->vertices_count = 0;
}


int find_vertex_idx( TrackVertexData* vertex_data, int piece_idx, int lane_idx, bool create_if_missing ) 
{
	cGraphVertex** vertices = vertex_data->vertices;

	int found_vtx = -1;
	for (int vertex_idx = 0; vertex_idx < vertex_data->vertices_count; vertex_idx++)
	{
		cGraphVertex* vtx = vertices[vertex_idx];
		if(cGraphVertex_matches(vtx, piece_idx, lane_idx))
		{
			found_vtx = vertex_idx;
			break;
		}
	}

	if (found_vtx == -1 && create_if_missing)
	{
		//not found. add it at the end of the vtx list.
		found_vtx = vertex_data->vertices_count;
		vertices[found_vtx] = cGraphVertex_create(piece_idx, lane_idx, Piece_Data_GetLaneLength(&g_trackData.pieces[piece_idx], lane_idx));
		vertex_data->vertices_count += 1;
	}

	return found_vtx;
}


cNode* get_unvisited_vertex(cList* list)
{
	cNode* currNode = list->head;
	while (currNode)
	{
		cGraphVertex* graphVtx = (cGraphVertex*)currNode->data;
		if (graphVtx->total_cost < INFINITY)
			break;
		currNode = currNode->next;
	}
	
	if (currNode)
		cNode_remove_from_list(list, currNode);
	
	
	return currNode;
}


void Dijkstra(cGraphVertex* start_vertex, cGraphVertex* end_vertex)
{
	start_vertex->total_cost = start_vertex->cost;
	cList* openList = cList_create();
	cList_tail_add(openList, start_vertex);
	while (cList_size(openList) > 0)
	{
		cNode* node = get_unvisited_vertex(openList);
		if (!node)
			break;
		cGraphVertex* vtx = (cGraphVertex*)node->data;
		
		//updateaza costul pt vertecsii din lista de adiacenta.
		cList* vtxList = vtx->list;
		cNode* otherNode = vtxList->head;
		while (otherNode)
		{
			cGraphVertex* other_node_vtx = (cGraphVertex*)otherNode->data;
			if (!other_node_vtx)
				continue;
			if (other_node_vtx->cost + vtx->total_cost < other_node_vtx->total_cost)
			{
				//update min cost and set the new parent.
				other_node_vtx->total_cost = other_node_vtx->cost + vtx->total_cost;
				other_node_vtx->parent = vtx;
			}
			
			if (!other_node_vtx->visited)
			{
				other_node_vtx->visited = true;
				cList_tail_add(openList, other_node_vtx);
			}
			
			otherNode = otherNode->next;
		}

		vtx->visited = true;
		cNode_destroy(node);
	}
	cList_destroy(openList);
}